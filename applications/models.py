from google.appengine.ext import ndb


class Post(ndb.Model):
    title = ndb.StringProperty()
    body = ndb.TextProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)
