import os
from flask import Flask

DEBUG = True
SECRET_KEY = os.urandom(512)

app = Flask(__name__)
app.config.from_object(__name__)

@app.route('/')
def home():
    return 'Hello world'

